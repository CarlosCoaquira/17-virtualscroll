Temas puntuales de la sección

En esta sección trataremos dos temas interesantes incluidos desde la versión 7 de Angular.

Principalmente nos enfocaremos en dos temas principales:

    Drag and Drop de Angular

    Virtual Scroll

Ambos son novedades de la versión, también vale la pena decir que si vienes de la versión 6 de Angular, no hay ningún cambio que rompa el código, si vienes de la versión 5 o inferior, recuerda que necesitarás actualizar tus RXJS o bien usar la versión de compatibilidad, pero en nuevos proyectos, se recomienda usar los nuevos RXJS versión 6 o superior.


Some language features are not available. To access all features, enable [strictTemplates](https://angular.io/guide/angular-compiler-options#stricttemplates) in [angularCompilerOptions](https://angular.io/guide/angular-compiler-options).


-- instalar cdk donde estan los componentes de virtual scroll, drag and drop, etc
npm install @angular/cdk


import { BrowserModule } from '@angular/platform-browser'; 
import { NgModule } from '@angular/core';  
import { ScrollingModule } from '@angular/cdk/scrolling'; 
import {DragDropModule} from '@angular/cdk/drag-drop';   
import { AppRoutingModule } from './app-routing.module'; 
import { AppComponent } from './app.component'; 
import { VirtualComponent } from './components/virtual/virtual.component'; 
import { DragComponent } from './components/drag/drag.component';  

@NgModule({   
	declarations: [     
		AppComponent,     
		VirtualComponent,     
		DragComponent   
	],   
	imports: [     
		BrowserModule,     
		AppRoutingModule,     
		ScrollingModule,     
		DragDropModule   
	],   
	providers: [],   
	bootstrap: [AppComponent] }) 
export class AppModule { }

-----------------------------------------------------------
Nota: Material para la siguiente clase
Nota para la siguiente clase:

En la siguiente clase trabajaremos con el Drag and Drop usando una lista ordenada, pero para que el ejercicio sea real, usaremos información de un servicio gratuito llamado RestCountries.

Abran y tengan a la mano este URL

https://restcountries.eu/rest/v2/lang/es


Si desean saber más información sobre esta API pueden consultar:

https://restcountries.eu

-----------------------------------------------------------
Más información sobre el Virtual Scroll y Drag & Drop de Angular

Documentaciones adicionales para complementar lo aprendido:


Aquí les dejo dos enlaces con más ejercicios y demos de los temas que vimos en esta sección:

https://material.angular.io/cdk/scrolling/overview
https://material.angular.io/cdk/drag-drop/overview
